//
//  ViewController4.swift
//  certificado
//
//  Created by Oscar Montemayor on 02/05/20.
//  Copyright © 2020 Sofia Montemayor. All rights reserved.
//

import UIKit

class ViewController4: UIViewController {

    
    let urlmap = URL(string: "https://www.google.com/maps/place/Publioos+Web+Services/@25.6709046,-100.3419918,17z/data=!4m5!3m4!1s0x8662be0256a3362f:0x2a89fda619eef62c!8m2!3d25.6707064!4d-100.3415841")
    @IBOutlet weak var map: UIButton!
    
    @IBAction func sentToMap(_ sender: AnyObject) {
        UIApplication.shared.open(URL(string: "https://www.google.com/maps/place/Publioos+Web+Services/@25.6709046,-100.3419918,17z/data=!4m5!3m4!1s0x8662be0256a3362f:0x2a89fda619eef62c!8m2!3d25.6707064!4d-100.3415841")! as URL,options: [:], completionHandler: nil)
    }
    
    @IBAction func openPublioos(_ sender: Any) {
        UIApplication.shared.open(URL(string: "http://www.publioos.com")! as URL,options: [:], completionHandler: nil)
    }
    
    
    
    override func viewDidLoad() {
        map.addTarget(self, action: "sentToMap", for: .touchUpInside)
        map.addTarget(self, action: "openPublioos", for: .touchUpInside)
        super.viewDidLoad()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
