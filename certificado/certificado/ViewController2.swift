//
//  ViewController2.swift
//  certificado
//
//  Created by Oscar Montemayor on 01/05/20.
//  Copyright © 2020 Sofia Montemayor. All rights reserved.
//

import UIKit

class ViewController2: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var showClients: UITableView!
    
    let clientArray = ["dummyClient1","dummyClient2","dummyClient3","dummyClient4","dummyClient5"]
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clientArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Clients", for: indexPath)
        cell.textLabel!.text = clientArray[indexPath.row]
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showClients.dataSource = self
        showClients.delegate = self
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
